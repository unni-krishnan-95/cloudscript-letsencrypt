location / {
    proxy_pass      $backend_protocol://$backend_host:$backend_port;
}