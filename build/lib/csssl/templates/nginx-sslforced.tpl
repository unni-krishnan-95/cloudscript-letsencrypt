server {
    listen       80;
    listen       [::]:80;
    server_name {{ servername }}{% if serveralias %} {{ serveralias }}{% endif %};
    location /.well-known {
        try_files $uri $uri/ =404;
    }
    return 301 https://$server_name$request_uri;
}

server {
    listen       443 ssl http2;
    listen       [::]:443 ssl http2;
    server_name {{ servername }}{% if serveralias %} {{ serveralias }}{% endif %};

    ssl_certificate_key      {{ sslpath }}/live/{{appname}}/privkey.pem;
    ssl_certificate          {{ sslpath }}/live/{{appname}}/fullchain.pem;

    root   /home/{{ username }}/webapps/{{ appname }};

    access_log  /home/{{ username }}/logs/{{ appname }}/nginx-cs/access.log  main;
    error_log  /home/{{ username }}/logs/{{ appname }}/nginx-cs/error.log;

    proxy_set_header    Host              $host;
    proxy_set_header    X-Real-IP         $remote_addr;
    proxy_set_header    X-Forwarded-For   $proxy_add_x_forwarded_for;
    proxy_set_header    X-Forwarded-SSL   on;
    proxy_set_header    X-Forwarded-Proto $scheme;

    include /etc/nginx-cs/vhosts.d/{{ appname }}.d/*.ssl_conf;
    include /etc/nginx-cs/vhosts.d/{{ appname }}.d/*.conf;
}
