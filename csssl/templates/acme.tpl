location ^~ /.well-known/acme-challenge/ {
    default_type "text/plain";
    rewrite /.well-known/acme-challenge/(.*) /$1 break;
    root /var/.rwssl/.well-known/acme-challenge/;
}