server {
    listen       80;
    listen       [::]:80;
    server_name {{ servername }}{% if serveralias %} {{ serveralias }}{% endif %};

    root   /home/{{ username }}/webapps/{{ appname }};

    access_log  /home/{{ username }}/logs/{{ appname }}/nginx-cs/access.log  main;
    error_log  /home/{{ username }}/logs/{{ appname }}/nginx-cs/error.log;

    proxy_set_header    Host              $host;
    proxy_set_header    X-Real-IP         $remote_addr;
    proxy_set_header    X-Forwarded-For   $proxy_add_x_forwarded_for;

    include /etc/nginx-cs/vhosts.d/{{ appname }}.d/*.nonssl_conf;
    include /etc/nginx-cs
    /vhosts.d/{{ appname }}.d/*.conf;
}
