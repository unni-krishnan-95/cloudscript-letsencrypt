from setuptools import setup
import os
import subprocess
from setuptools.command.install import install
import shutil
import sys

class SetupSslRenewCron(install):
	def run(self):
		crondir = '/etc/cron.weekly'
		cronfile = os.path.join(crondir, 'csssl-sslrenewals')
		if not os.path.exists(crondir):
			os.makedirs(crondir)
		try:
			cmd = '%s renew --non-interactive --config-dir etc/nginx-cs/le-ssls --post-hook "service nginx-cs reload"\n' % shutil.which('certbot')
		except:
			# which() on shutil module is not available under Python 3.x
			sys.exit('Looks like you are running an older version of Python. Only Python 3.x is supported.')

		with open(cronfile, 'w') as cf:
			cf.writelines(['#!/bin/sh\n', cmd])
		maxexeccmd = "chmod +x {}".format(cronfile)
		FNULL = open(os.devnull, 'w')
		subprocess.check_call([maxexeccmd], shell=True, stdout=FNULL, stderr=subprocess.STDOUT)
		install.run(self)

setup(name='csssl',
	version='2.0.0',
	description='A Python package to manage Let\'s Encrypt SSL on cloudstick provisioned servers.',
	author='CloudStick',
	author_email='packaging@cloudstick.io',
	license='MIT',
	python_requires='>3.5.2',
	entry_points={
		'console_scripts': [
			'csssl = csssl.csssl:main'
			],
	},
	packages=[
		'csssl'
	],
	install_requires=[
		'python-nginx',
		'validators',
		'termcolor',
		'tabulate',
		'Jinja2',
		'certbot'
	],
	package_data={'csssl': ['templates/*.tpl']},
	cmdclass={
		'install': SetupSslRenewCron
	}
)
